MeiweiApp.i18n = {
    'Circles': {
        zh: '全部商圈',
        en: 'Circles'
    },
    'Cuisines': {
        zh: '全部菜系',
        en: 'Cuisines'
    },
    'Recommends': {
        zh: '美位榜单',
        en: 'Recommends'
    },
    'Next Page': {
        zh: '下一页',
        en: 'Next Page'
    },
    'Previous Page': {
        zh: '上一页',
        en: 'Previous Page'
    },
    'Reserve': {
        zh: '订餐',
        en: 'Book'
    },
    'Price Range': {
        zh: '人均',
        en: 'Price'
    },
    'Reviews': {
        zh: '评论',
        en: 'Reviews'
    },
    'Call Us': {
        zh: '客服电话',
        en: 'Call Us'
    },
    'Mr.': {
        zh: '先生',
        en: 'Mr.'
    },
    'Ms.': {
        zh: '小姐',
        en: 'Ms.'
    },
    'Reservation Detail': {
        zh: '预订信息',
        en: 'Reservation Detail'
    },
    'Select Contact': {
        zh: '选择预订人',
        en: 'Select Contact'
    },
    'Select Seat': {
        zh: '桌位选择',
        en: 'Select Seat'
    },
    'Seat Selected': {
        zh: '已选',
        en: 'Seat Selected'
    },
    'Submit Order': {
        zh: '提交订单',
        en: 'Submit Order'
    },
    'Floorplan': {
        zh: '桌位',
        en: 'Floorplan'
    },
    'Remarks': {
        zh: '备注',
        en: 'Remarks'
    },
    'Member Center': {
        zh: '用户中心',
        en: 'Member Center'
    },
    'My': {
        zh: '我的',
        en: 'My '
    },
    'Profile': {
        zh: '资料',
        en: 'Profile'
    },
    'Reservations': {
        zh: '订单',
        en: 'Reservations'
    },
    'Credits': {
        zh: '积分',
        en: 'Credits'
    },
    'Favorites': {
        zh: '收藏',
        en: 'Favorites'
    },
    'Meiwei Concierge': {
        zh: '美位私人管家',
        en: 'Meiwei Concierge'
    },
    'Concierge': {
        zh: '私人管家',
        en: 'Concierge'
    },
    'Concierge Services': {
        zh: '私人管家服务',
        en: 'Concierge Services'
    },
    'Anniversaries': {
        zh: '纪念日',
        en: 'Anniversaries'
    },
    'Logout': {
        zh: '退出登录',
        en: 'Logout'
    },
    'Already Have an Account?': {
        zh: '已经注册？',
        en: 'Already Have an Account?'
    },
    'Or Create Your Account': {
        zh: '或者新建一个帐号',
        en: 'Or Create Your Account'
    },
    'Login': {
        zh: '登录',
        en: 'Login'
    },
    'Register': {
        zh: '注册',
        en: 'Register'
    },
    'Social Login': {
        zh: '公共帐号登录',
        en: 'Social Login'
    },
    'Name': {
        zh: '姓名',
        en: 'Name'
    },
    'Email': {
        zh: '邮箱',
        en: 'Email'
    },
    'Mobile': {
        zh: '手机',
        en: 'Mobile'
    },
    'Mobile/Email': {
        zh: '手机/邮箱',
        en: 'Mobile/Email'
    },
    'Birthday': {
        zh: '生日',
        en: 'Birthday'
    },
    'Save': {
        zh: '保存',
        en: 'Save'
    },
    'Modify Password': {
        zh: '修改密码',
        en: 'Modify Password'
    },
    'Confirm Password': {
        zh: '确认密码',
        en: 'Confirm Password'
    },
    'Password': {
        zh: '密码',
        en: 'Password'
    },
    'Contact': {
        zh: '联系人',
        en: 'Contact'
    },
    'Contacts': {
        zh: '联系人',
        en: 'Contacts'
    },
    'Online Contacts': {
        zh: '美位联系人',
        en: 'Online Contacts'
    },
    'Local Contacts': {
        zh: '手机通讯录',
        en: 'Local Contacts'
    },
    'Gifts': {
        zh: '兑换礼品',
        en: 'Gifts'
    },
    'Anniversary': {
        zh: '纪念日',
        en: 'Anniversary'
    },
    'Add': {
        zh: '添加',
        en: 'Add'
    },
    'Title': {
        zh: '标题',
        en: 'Title'
    },
    'Date': {
        zh: '日期',
        en: 'Date'
    },
    'Delete': {
        zh: '删除',
        en: 'Delete'
    },
    'Pending Orders': {
        zh: '未完成订单',
        en: 'Pending Orders'
    },
    'Fulfilled Orders': {
        zh: '已完成订单',
        en: 'Fulfilled Orders'
    },
    'Detail': {
        zh: '详情',
        en: 'Detail'
    },
    'CheckinAndShare': {
        zh: '签到 &amp; 分享',
        en: 'Checkin &amp; Share'
    },
    'Checkin': {
        zh: '签到',
        en: 'Checkin'
    },
    'Settings': {
        zh: '设置',
        en: 'Settings'
    },
    'Language': {
        zh: '语言',
        en: 'Language'
    },
    'Search Hint': {
        zh: '输入关键字搜索餐厅',
        en: 'Search Restaurants'
    },
    'Search Contacts Hint': {
        zh: '输入姓名',
        en: 'Search Contacts'
    },
    'Book Now': {
        zh: '赶紧预订',
        en: 'Book Now'
    },
    'Your pending orders will be here': {
    	zh: '您将要出席的餐厅订单会显示在这里',
    	en: 'Your pending orders will be here'
    },
    "Password doesn't match.": {
        zh: '两次密码输入不一致，请重新输入。',
        en: "Password doesn't match."
    },
    'Please confirm the cancellation': {
        zh: '请确认取消订单',
        en: 'Cancel Order'
    },
    'An SMS will be sent to you to inform you the order has been confirmed': {
        zh: '订单被确认以后您会收到一条短信',
        en: 'An SMS will be sent to you to inform you the order has been confirmed'
    },
    'Find your order in the top-right corner on homepage. Please show your order to the waiting staff in order to get better service.': {
        zh: '点击首页右上角可以快速查看订单。为了更好的为您服务，到店后请向店员出示订单。',
        en: 'Find your order in the top-right corner on homepage. Please show your order to the waiting staff in order to get better service.'
    },
    'Cancel Order': {
        zh: '取消订单',
        en: 'Cancel Order'
    },
    'Confirm Order': {
        zh: '确认订单',
        en: 'Confirm Order'
    },
    'Confirm': {
        zh: '确认',
        en: 'Confirm'
    },
    'Cancel': {
        zh: '取消',
        en: 'Cancel'
    },
    'Close': {
        zh: '关闭',
        en: 'Close'
    },
    'Yes': {
        zh: '是',
        en: 'Yes'
    },
    'No': {
        zh: '否',
        en: 'No'
    },
    'Order No': {
        zh: '订单号',
        en: 'Order No'
    },
    'Order Date': {
        zh: '订餐日期',
        en: 'Date'
    },
    'Order Time': {
        zh: '订餐时间',
        en: 'Time'
    },
    'Peoples': {
        zh: '人数',
        en: 'Peoples'
    },
    'Discount': {
        zh: '折扣',
        en: 'Discount'
    },
    'Share to Moments': {
        zh: '分享到朋友圈',
        en: 'Share to Moments'
    },
    'Share to Weibo': {
        zh: '分享到微博',
        en: 'Share to Weibo'
    },
    'Purchase': {
        zh: '兑换',
        en: 'Purchase'
    },
    'credits left. One of our customer service will contact you as soon as possible.': {
        zh: '剩余积分。我们的客服会马上联系您。',
        en: ' credits left. One of our customer service will contact you as soon as possible.'
    },
    'Successfully! You have': {
        zh: '兑换成功！您还有',
        en: 'Successfully! You have '
    },
    'Share Meiwei with your friends': {
        zh: '和朋友们分享美位',
        en: 'Share Meiwei with your friends'
    },
    'You will receive a gift after sharing': {
        zh: '分享后可以得到精美礼品一份',
        en: 'You will receive a gift after sharing'
    },
    'Update Available': {
        zh: '更新提示',
        en: 'Update Available'
    },
    'Version': {
        zh: '版本',
        en: 'Version'
    },
    'New version is available, go to update?': {
        zh: '好消息！美位手机应用更新啦，前往下载？',
        en: 'New version is available, go to update?'
    },
    'Restart Application': {
        zh: '重启应用',
        en: 'Restart Application'
    },
    'An application restart is required to apply language setting. Restart now?': {
        zh: '重启应用后语言设置才能生效。现在就重启？',
        en: 'An application restart is required to apply language setting. Restart now?'
    },
    'Show More Restaurants': {
        zh: '更多餐厅',
        en: 'More Restaurants'
    },
    'No Discount': {
        zh: '无折扣',
        en: 'No Discount'
    },
    'Click for Detail': {
        zh: '点击查看详情',
        en: 'Click for Detail'
    },
    'Weekly Choice': {
    	zh: '本周餐厅',
    	en: 'Weekly Choice'
    },
    'Designated Driving': {
        zh: '代驾服务',
        en: 'Designated Driving'
    },
    'Thanks to 4001002003.com': {
        zh: '感谢安师傅提供代驾服务',
        en: 'The service is provided by our partner www.4001002003.com'
    },
    'Our driver will contact you as soon as possible': {
        zh: '我们会尽快安排司机和您联系',
        en: 'Our driver will contact you as soon as possible'
    },
    'Book Another': {
        zh: '继续下单',
        en: 'Book Another'
    }
};

MeiweiApp.CheckI18n = function() {
    var newMsg = [];
    var checkExist = function(el) {
        var msg = $(el).attr('data-i18n');
        if (msg && !MeiweiApp.i18n[msg]) newMsg.push(msg);
        msg = $(el).attr('data-placeholder-i18n');
        if (msg && !MeiweiApp.i18n[msg]) newMsg.push(msg);
    }
    $('[data-i18n], [data-placeholder-i18n]').each(function() {
        checkExist(this);
    });
    for (var i in TPL) {
        var template = TPL[i]();
        $(template).find('[data-i18n], [data-i18n-placeholder]').each(function() {
            checkExist(this);
        });
        $(template).filter('[data-i18n], [data-i18n-placeholder]').each(function() {
            checkExist(this);
        });
    }
    return newMsg;
};
