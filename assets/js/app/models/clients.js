
MeiweiApp.Models.ClientError = MeiweiApp.Model.extend({
	urlRoot: MeiweiApp.configs.APIHost + '/clients/error/'
});

MeiweiApp.Models.App = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/clients/app/'
});

MeiweiApp.Models.Ad = MeiweiApp.Model.extend({
    urlRoot: MeiweiApp.configs.APIHost + '/clients/ad/'
});
